//
//  main.m
//  SSCODE
//
//  Created by wei_yijie on 16/4/5.
//  Copyright © 2016年 showsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
